#OracleBot
#Copyright (C) 2017  Tom Godden
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

from random import seed, random
import numbers
seed()

class ParseException(Exception):
    pass
class UnclosedBracketException(ParseException):
    pass

def get_operation(op):
    def minus_function(a,b):
        if(a == None):
            return -b.evaluate()
        else:
            return a.evaluate()-b.evaluate()
    operations = {
            "+": lambda a,b: a.evaluate()+b.evaluate(),
            "-":  minus_function,
            "/": lambda a,b: a.evaluate()/b.evaluate(),
            "*": lambda a,b: a.evaluate()*b.evaluate(),
            }
    if(op.startswith("d")):
        number = op[1:]
        return int(random() * int(number)) + 1
    else:
        return operations[op]

class Node():
    def __init__(self, value, leaf = False, left = None, right = None):
        self.leaf = leaf
        self.left = left
        self.right = right
        self.dice = []
        try:
            self.value = get_operation(value)
            if isinstance(self.value, numbers.Number):#It was a die
                self.dice  += [self.value]
        except KeyError:
            self.value = float(value) #It was a number

    def evaluate(self):
        if self.leaf:
            return self.value
        else:
            val = self.value(self.left, self.right)
            if(self.left != None):
                self.dice += self.left.dice
            if(self.right != None):
                self.dice += self.right.dice
            return val 
    def __str__(self):
        if self.leaf:
            return str(self.value)
        else:
            return "(" + str(self.left) + str(self.value) + str(self.right) + ")"
                
class _Grouper():
    def __init__(self, expression):
        self.expression = expression
        self.groups = []
        self.current_group = ""
        self.die_group = ""
        self.bracket_stack = 0
        self.procs = self._create_dict()
        self._make_groups()

    def _end_group_implicitly(self):
        """
        Ends the group, adding the implicit multiplication.
        """
        if(self.die_group != ""):
            self.groups.append(self.die_group)
            self.die_group = ""
            self.groups.append("*")
        elif(self.current_group  != ""):
            self.groups.append(self.current_group)
            self.current_group = ""
            self.groups.append("*")

    def _end_group_explicitly(self, operator):
        """
        Ends the group, adding the operator.
        """
        if(self.die_group != ""):
            self.groups.append(self.die_group)
            self.die_group = ""
        elif(self.current_group  != ""):
            self.groups.append(self.current_group)
            self.current_group = ""
        self.groups.append(operator)

    def _finish_parsing(self):
        """
        Cleans up leftover characters.
        """
        if self.bracket_stack != 0:
            raise UnclosedBracketException
        if self.current_group != "":
            self.groups.append(self.current_group)
        elif self.die_group != "":
            self.groups.append(self.die_group)

    def _check_brackets(self, c):
        """
        Checks the brackets
        """
        if self.bracket_stack == 0:
            return True
        else:
            self.current_group += c
            return False

    #----- Operation per character -----#
    def _left_bracket(self, c): # (
        self.bracket_stack += 1
        self._end_group_implicitly() #End group, maybe add an implicit multiplication

    def _right_bracket(self, c): # )
        self.bracket_stack -= 1
        if(self.bracket_stack < 0): #More right brackets than left
            raise UnclosedBracketException
        if(self.bracket_stack == 0): #Completely unbracketed
            self.groups.append(_Grouper(self.current_group+self.die_group).groups)
            self.current_group = ""
            self.die_group = ""
            
    def _operator(self, c): # + - * /
        if self._check_brackets(c):
            self._end_group_explicitly(c) #End group and add an operator
    
    def _dice(self, c): # d D
        if self._check_brackets(c):
            self._end_group_implicitly() #End group, maybe add an implicit multiplication
            self.die_group += "d"

    def _number(self, c): # 0-9 .
        if self.die_group == "": #Not working on a die
            self.current_group += c
        else: #Working on a die
            self.die_group += c

    def _space(self, c): #  
        pass

    def _not_in_dict(self, c):
        print("ERROR: Illegal value while parsing: " + c)
        raise ValueError
    #----- The Dictionary -----#
    def _create_dict(self):
        d = {
            "(": self._left_bracket,
            ")": self._right_bracket,
            "d": self._dice,
            " ": self._space,
            }
        d.update(dict.fromkeys(["+", "-", "*", "/"], self._operator))
        d.update(dict.fromkeys([str(m) for m in list(range(0,10))] + ["."], self._number))
        return d

    def _get_from_dict(self, c):
        try:
            return self.procs[c]
        except ValueError:
            return self._not_in_dict(c)


    def _make_groups(self):
        """
        Convert the expression to an array (group),
        consisting of operators, numbers and other groups.

        Parts of the expression in brackets will be converted 
        to another group. This nesting can happen multiple times.
        """
        for c in self.expression:
            self._get_from_dict(c)(c)
            #END OF PARSING LOOP
        self._finish_parsing()


class Tree():
    def __init__(self, expression):
        self.expression = expression
        self.root = self._parse()
        self._evaluated = False
    
    def _make_groups(self, expression):
        groups = _Grouper(expression).groups
        return groups

    def _parse_groups(self, groups):
        """
        Convert the nested groups into a tree structure,
        according to order of operations.
        """
        if(len(groups) > 1): #Is a list of items
            #Search for + and -
            i = len(groups) - 1
            while (i >= 0) and ((groups[i] != "+") and (groups[i] != "-")):
                i-=1
            if i < 0: #No + or - found
                #Search for * and /
                i = len(groups) - 1
                while (i >= 0) and ((groups[i] != "*") and (groups[i] != "/")):
                    i-=1
            #An operator should have been found, so create root
            if(i >= 0):
                left = self._parse_groups(groups[:i])
                right = self._parse_groups(groups[i+1:])
                op = groups[i]
                return Node(value = op, left = left, right = right)
            else:
                print(groups)
                raise IndexError(i)
        elif(len(groups) == 1): #Is a single element, not empty
            element = groups[0]
            if isinstance(element, list): #Is between ( )
                return self._parse_groups(element)
            else:
                return Node(value = element, leaf = True)

    def _parse(self):
        """
        Do the grouping and parsing of grouping.
        """
        if self.expression == "":
            raise ParseException
        root = self._parse_groups(self._make_groups(self.expression))
        return root

    def evaluate(self):
        """
        Evaluate the tree.
        """
        self._evaluated = True
        return self.root.evaluate()

    def get_dice_rolls(self):
        """
        Get the dice rolled during evaluation.
        """
        if not self._evaluated:
            self.evaluate()
        return self.root.dice
