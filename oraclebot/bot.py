#OracleBot
#Copyright (C) 2017  Tom Godden
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

import discord
import asyncio
import sys
from oracleBotCommands import commands, evaluate_expression
from common.parser import parse_message
client = discord.Client()

@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')

@client.event
async def on_message(message):
    if(message.content[0] == "="):
        await evaluate_expression(client, message)
    else:
        parsed = parse_message(message.content)
        command = commands.get_command(parsed[0])
        if len(parsed) > 1:
            args = parsed[1:]
        else:
            args = []
        if(command != None):
            await command.do(client, message, args)

def main(argv):
    try:
        token = argv[1]
    except IndexError:
        print("ERROR: you need to pass bot token as argument")
        sys.exit(-1)
    client.run(token)

if __name__ == "__main__":
    main(sys.argv)
