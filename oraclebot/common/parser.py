#OracleBot
#Copyright (C) 2017  Tom Godden
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re

def parse_message(message_string):
    matches = re.split("[ ]", message_string) 
    matches = list(filter(None, matches))
    matches = [m.strip() for m in matches]
    return matches

if __name__ == "__main__":
    print(parse_message("!foo"))
    print(parse_message("!bar 0 1 2"))
    print(parse_message("!oof(dniuozaod kzopakpdoé'è è!&, 2, 3 )"))
