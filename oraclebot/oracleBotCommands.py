#OracleBot
#Copyright (C) 2017  Tom Godden
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

from common.commands import Command, CommandList
import common.math_parser as mp
import asyncio
import discord
from random import random, gauss, seed

seed()

#----- Commands -----#

def generate_insult():
    adjectives = ["bloody", "stinking", "disgusting", "frog-eating", "feeding",
            "brainless", "disloyal", "garlic-breathed", ]
    insults = ["dimwit", "numbskull", "Frenchman", "dolt", "oaf",
            "rapscallion", "cockroach", "maggot", "trashpanda", ]
    amount_of_adjectives = int(max(-1, gauss(0, 2))) + 1
    adjective_string = ""
    for i in range(amount_of_adjectives):
        adjective_string += adjectives[int(len(adjectives)*random())]
        if(i < amount_of_adjectives - 1):
            adjective_string += ","
        adjective_string += " "

    chosen_insult = insults[int(len(insults)*random())]
    return adjective_string + chosen_insult

async def help_command_f(client, message, args):
    await client.send_message(message.channel, commands.list_commands())

async def source_command_f(client, message, args):
    await client.send_message(message.channel,
            "Source code is available at https://gitlab.com/tgodden/OracleBot")



async def d_command_f(client, message, args):
    try:
        d = int(args[0])
        await client.send_message(message.channel, int(random()*d) + 1)
    except ValueError:
        await client.send_message(message.channel, "Pass a number you " + generate_insult() + "!")
    except IndexError:
        await client.send_message(message.channel, "Pass a number you " + generate_insult() + "!")

async def insult_command_f(client, message, args):
    author = message.author
    name = author.display_name
    try:
        target = " ".join(args)
        await client.send_message(message.channel, name + " thinks " + target + " is a " + generate_insult() + "!")
    except IndexError:
        await client.send_message(message.channel, "Get your shit together, " + name + "!")

commands = CommandList([
    ("!help", Command(help_command_f, description="display this message", visible = True)),
    ("!d", Command(d_command_f,
        args = ["sides"],
        description="throw a die", visible=True)),
    ("!insult", Command(insult_command_f)),
    ("!source", Command(source_command_f, description="get source code", visible = True)),
        ])

#------ Evaluation of expressions -----#

async def evaluate_expression(client, message):
    expression = message.content[1:]
    try:
        tree = mp.Tree(expression)
        value = str(tree.evaluate())
        dice = [str(d) for d in tree.get_dice_rolls()]
        if(dice != []):
            value += " (" + ", ".join(dice) + ")"
        await client.send_message(message.channel, value)
    except(mp.ParseException, ValueError):
        await client.send_message(message.channel, "Didn't understand that, " + message.author.display_name + "!")

