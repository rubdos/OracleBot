#OracleBot
#Copyright (C) 2017  Tom Godden
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

class CommandList():

    def __init__(self, string_command_pairs = []):
        self.dict = {}
        self.add_commands(string_command_pairs)

    def get_command(self, command_string):
        return self.dict.get(command_string)

    def add_command(self, command_string, command):
        self.dict[command_string] = command

    def add_commands(self, string_command_pairs):
        for string, command in string_command_pairs:
            self.add_command(string, command)

    def list_commands(self):
        result = ""
        for key, value in sorted(self.dict.items()):
            if(value.visibility):
                result += "\n**" + key + "** " + "".join("*{0}* ".format(a) for a in value.args) +\
                        ": " + value.description
        return result


class Command():
    
    def __init__(self, to_execute, args = [], description = "No description", visible = False):
        self.do = to_execute
        self.args = args
        self.description = description
        self.visibility = visible
